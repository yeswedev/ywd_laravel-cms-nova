<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Resources Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default names of resources.
    |
    */
    'archive_names'     => [
        'archive_label'         => 'Listes',
        'archive_singularlabel' => 'Liste',
    ],
    'fichier_names'     => [
        'fichier_label'         => 'Fichiers',
        'fichier_singularlabel' => 'Fichier',
    ],
    'bloc_names'        => [
        'bloc_label'         => 'Blocs',
        'bloc_singularlabel' => 'Bloc',
    ],
    'post_names'        => [
        'post_label'         => 'Pages',
        'post_singularlabel' => 'Page',
    ],
    'filtre_names'      => [
        'filtre_label'         => 'Filtres',
        'filtre_singularlabel' => 'Filtre',
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources Group Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default group names of resources.
    |
    */
    'resources'         => [
        'archive'    => [
            'group' => 'Other',
        ],
        'attachment' => [
            'group' => 'Other',
        ],
        'block'      => [
            'group' => 'Other',
        ],
        'post'       => [
            'group' => 'Other',
        ],
        'tag'        => [
            'group' => 'Other',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources Register and Display
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default register and display configuration of resources.
    |
    */
    'archive_resource'  => [
        'archive_register' => true,
        'archive_display'  => true,
    ],
    'fichier_resource'  => [
        'fichier_register' => true,
        'fichier_display'  => false,
    ],
    'bloc_resource'     => [
        'bloc_register' => true,
        'bloc_display'  => false,
    ],
    'post_resource'     => [
        'post_register' => true,
        'post_display'  => true,
    ],
    'filtre_resource'   => [
        'filtre_register' => true,
        'filtre_display'  => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Fields Display
    |--------------------------------------------------------------------------
    |
    | For each resource, select the fields shown in the back-office.
    |
    */
    'fields'            => [
        'archive'    => [
            'title'       => true,
            'description' => true,
            'location'    => true,
            'publication' => true,
            'reference'   => true,
            'url'         => true,
            'priority'    => true,
            'attachments' => true,
            'blocks'      => true,
            'tags'        => true,
            'children'    => true,
            'parent'      => true,
            'wysiwyg'     => 'Trix'
        ],
        'attachment' => [
            'src'         => true,
            'title'       => true,
            'description' => true,
            'parent'      => true,
            'wysiwyg'     => 'Trix'
        ],
        'block'      => [
            'layout'         => true,
            'title'          => true,
            'description'    => true,
            'priority'       => true,
            'layout_options' => [
                'block-full-width' => 'Block texte de largeur 100%'
            ],
            'attachments'    => true,
            'parent'         => true,
            'wysiwyg'        => 'Trix'
        ],
        'post'       => [
            'title'       => true,
            'title_name'  => 'Titre',
            'description' => true,
            'location'    => true,
            'publication' => true,
            'reference'   => true,
            'url'         => true,
            'priority'    => true,
            'index'       => [
                'title'       => true,
                'description' => true,
                'location'    => true,
                'url'         => true,
            ],
            'attachments' => true,
            'blocks'      => true,
            'tags'        => true,
            'parent'      => true,
            'wysiwyg'     => 'Trix'
        ],
        'tag'        => [
            'title'       => true,
            'description' => true,
            'parent_archives' => true,
            'parent_posts' => true,
            'wysiwyg'     => 'Trix'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Attachments types
    |--------------------------------------------------------------------------
    |
    | Set the list of allowed types for attachments.
    |
    */
    'attachments-types' => [
        'banner'    => 'Bannière',
        'document'  => 'Document',
        'icon'      => 'Icône',
        'img'       => 'Image',
        'thumbnail' => 'Miniature',
    ],

    'seo-fields' => false,

    'seo-fields-options' => [
        'seo_h1' => false,
        'seo_title' => false,
        'seo_description' => false,
    ],

];
