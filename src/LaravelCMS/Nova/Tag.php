<?php

namespace YesWeDev\LaravelCMS\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Resource;

use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Text;
use YesWeDev\Nova\Translatable\Translatable;

use YesWeDev\LaravelCMS\Tag as TagBase;
use Waynestate\Nova\CKEditor;

class Tag extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = TagBase::class;

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-cms-nova.resources.tag.group');
    }

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('laravel-cms-nova.filtre_resource')['filtre_display'];
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-cms-nova.filtre_names')['filtre_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-cms-nova.filtre_names')['filtre_singularlabel'];
    }

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = [ 'translations' ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [];
        if (config('laravel-cms-nova.fields.tag.title')) {
            $fields[] = Translatable::make('Nom', 'title')
                                    ->singleLine();
        }
        if (config('laravel-cms-nova.fields.tag.description')) {
            if (config('laravel-cms-nova.fields.tag.wysiwyg') === 'CKEditor') {
                $fields[] = CKEditor::make('Description', 'description')
                    ->options([
                        'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                    ]);
            } else {
                $fields[] = Translatable::make('Description', 'description')
                    ->trix();
            }
        }

        if (config('laravel-cms-nova.fields.tag.parent_archives') && config('laravel-cms-nova.archive_resource.archive_register')) {
            $fields[] = BelongsToMany::make(Archive::label(), 'archives', Archive::class);
        }
        if (config('laravel-cms-nova.fields.tag.parent_posts') && config('laravel-cms-nova.post_resource.post_register')) {
            $fields[] = BelongsToMany::make(Post::label(), 'posts', Post::class);
        }

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
