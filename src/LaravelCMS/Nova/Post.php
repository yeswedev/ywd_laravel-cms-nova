<?php

namespace YesWeDev\LaravelCMS\Nova;

use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use YesWeDev\Nova\Translatable\Translatable;

use YesWeDev\LaravelCMS\Post as PostBase;
use Waynestate\Nova\CKEditor;

class Post extends Page
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = PostBase::class;

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-cms-nova.resources.post.group');
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return parent::indexQuery($request, $query)->where('type', 'post');
    }

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('laravel-cms-nova.post_resource')['post_display'];
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-cms-nova.post_names')['post_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-cms-nova.post_names')['post_singularlabel'];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $tabs = [
        'Détails' => []
      ];
        if (config('laravel-cms-nova.fields.post.title')) {
            if (config('laravel-cms-nova.fields.post.index.title')) {
                $tabs['Détails'][] = Translatable::make(config('laravel-cms-nova.fields.post.index.title_name', 'Titre'), 'title')
                                        ->singleLine();
            } else {
                $tabs['Détails'][] = Translatable::make(config('laravel-cms-nova.fields.post.index.title_name', 'Titre'), 'title')
                                        ->singleLine()->hideFromIndex();
            }
        }
        if (config('laravel-cms-nova.fields.post.description')) {
            if (config('laravel-cms-nova.fields.post.index.description')) {
                if (config('laravel-cms-nova.fields.post.wysiwyg') === 'CKEditor') {
                    $tabs['Détails'][] = CKEditor::make('Contenu', 'description')
                        ->options([
                            'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                        ]);
                } else {
                    $tabs['Détails'][] = Translatable::make('Contenu', 'description')
                        ->trix();
                }
            } else {
                if (config('laravel-cms-nova.fields.post.wysiwyg') === 'CKEditor') {
                    $tabs['Détails'][] = CKEditor::make('Contenu', 'description')
                        ->options([
                            'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                        ])
                        ->hideFromIndex();
                } else {
                    $tabs['Détails'][] = Translatable::make('Contenu', 'description')
                        ->trix()->hideFromIndex();
                }
            }
        }
        if (config('laravel-cms-nova.fields.post.location')) {
            if (config('laravel-cms-nova.fields.post.index.location')) {
                $tabs['Détails'][] = Translatable::make('Lieu', 'location')
                                        ->singleLine();
            } else {
                $tabs['Détails'][] = Translatable::make('Lieu', 'location')
                                        ->singleLine()->hideFromIndex();
            }
        }
        if (config('laravel-cms-nova.fields.post.url')) {
            if (config('laravel-cms-nova.fields.post.index.url')) {
                $tabs['Détails'][] = Translatable::make('Url', 'url')
                                        ->singleLine();
            } else {
                $tabs['Détails'][] = Translatable::make('Url', 'url')
                                        ->singleLine()->hideFromIndex();
            }
        }
        if (config('laravel-cms-nova.fields.post.reference')) {
            $tabs['Détails'][] = Text::make('Référence', 'reference')
                            ->sortable();
        }
        if (config('laravel-cms-nova.fields.post.publication')) {
            $tabs['Détails'][] = Date::make('Date de publication', 'publication')
                            ->sortable();
        }
        if (config('laravel-cms-nova.fields.post.priority')) {
            $tabs['Détails'][] = Number::make('Priorité', 'priority')
                              ->step(0.01)
                              ->sortable();
        }

        if (config('laravel-cms-nova.fields.post.attachments') && config('laravel-cms-nova.fichier_resource.fichier_register')) {
            $tabs['Détails'][] = MorphMany::make(Attachment::label(), 'attachments', Attachment::class);
        }
        if (config('laravel-cms-nova.fields.post.blocks') && config('laravel-cms-nova.bloc_resource.bloc_register')) {
            $tabs['Détails'][] = HasMany::make(Block::label(), 'blocks', Block::class);
        }
        if (config('laravel-cms-nova.fields.post.tags') && config('laravel-cms-nova.filtre_resource.filtre_register')) {
            $tabs['Détails'][] = MorphToMany::make(Tag::label(), 'tags', Tag::class);
        }
        if (config('laravel-cms-nova.fields.post.parent') && config('laravel-cms-nova.archive_resource.archive_register')) {
            $tabs['Détails'][] = BelongsTo::make(Archive::singularLabel(), 'parent', Archive::class)->hideFromIndex();
        }

        if (config('laravel-cms-nova.seo-fields')) {
            $tabs['Onglet SEO'] = [];

            if (config('laravel-cms-nova.seo-fields-options.seo_h1')) {
                $tabs['Onglet SEO'][] = Text::make('H1', 'seo_h1')->hideFromIndex();
            }

            if (config('laravel-cms-nova.seo-fields-options.seo_title')) {
                $tabs['Onglet SEO'][] = Text::make('Méta Titre', 'seo_title')->hideFromIndex();
            }

            if (config('laravel-cms-nova.seo-fields-options.seo_description')) {
                $tabs['Onglet SEO'][] = Text::make('Méta Description', 'seo_description')->rules("max:160")->hideFromIndex();
            }
        }

        return [new Tabs('Tabs', $tabs)];
    }
}
