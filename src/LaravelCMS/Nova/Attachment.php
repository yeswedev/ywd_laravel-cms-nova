<?php

namespace YesWeDev\LaravelCMS\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Resource;

use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use YesWeDev\Nova\Translatable\Translatable;

use YesWeDev\LaravelCMS\Attachment as AttachmentBase;
use Waynestate\Nova\CKEditor;

class Attachment extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = AttachmentBase::class;

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-cms-nova.resources.attachment.group');
    }

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('laravel-cms-nova.fichier_resource')['fichier_display'];
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-cms-nova.fichier_names')['fichier_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-cms-nova.fichier_names')['fichier_singularlabel'];
    }

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = [ 'translations' ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'type';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'type',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [
            Select::make('Type', 'type')
                  ->options(config('laravel-cms-nova.attachments-types'))
                  ->displayUsingLabels()
                  ->rules('required')
                  ->sortable(),
        ];
        if (config('laravel-cms-nova.fields.attachment.src')) {
            $fields[] = Image::make('Fichier', 'src')
                             ->disk('public')
                             ->storeAs(function (Request $request) {
                                 $originalName = $request->src->getClientOriginalName();
                                 $timestamp = time();
                                 $name = preg_replace('/(\.[^.]*$)/', "-".time()."$1", $originalName);
                                 return $name;
                             })
                             ->prunable();
        }
        if (config('laravel-cms-nova.fields.attachment.title')) {
            $fields[] = Translatable::make('Nom', 'title')
                                    ->singleLine();
        }
        if (config('laravel-cms-nova.fields.attachment.description')) {
            if (config('laravel-cms-nova.fields.attachment.wysiwyg') === 'CKEditor') {
                $fields[] = CKEditor::make('Description', 'description')
                    ->options([
                        'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                    ]);
            } else {
                $fields[] = Translatable::make('Description', 'description')
                    ->trix();
            }
        }

        if (config('laravel-cms-nova.fields.attachment.parent') &&
             config('laravel-cms-nova.archive_resource.archive_register') &&
             config('laravel-cms-nova.bloc_resource.bloc_register') &&
             config('laravel-cms-nova.post_resource.post_register')) {
            $fields[] = MorphTo::make('Parent', 'owner')->types([
                Archive::class,
                Block::class,
                Post::class,
            ]);
        }

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
