<?php

namespace YesWeDev\LaravelCMS\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Resource;

use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use YesWeDev\Nova\Translatable\Translatable;

use YesWeDev\LaravelCMS\Block as BlockBase;
use Waynestate\Nova\CKEditor;

class Block extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = BlockBase::class;

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-cms-nova.resources.block.group');
    }

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('laravel-cms-nova.bloc_resource')['bloc_display'];
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-cms-nova.bloc_names')['bloc_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-cms-nova.bloc_names')['bloc_singularlabel'];
    }

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = [ 'translations' ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [];
        if (config('laravel-cms-nova.fields.block.layout')) {
            $fields[] = Select::make('Disposition', 'layout')->options(config('laravel-cms-nova.fields.block.layout_options'))->rules('required');
        }
        if (config('laravel-cms-nova.fields.block.title')) {
            $fields[] = Translatable::make('Titre', 'title')
                                    ->singleLine();
        }
        if (config('laravel-cms-nova.fields.block.description')) {
            if (config('laravel-cms-nova.fields.block.wysiwyg') === 'CKEditor') {
                $fields[] = CKEditor::make('Contenu', 'description')
                    ->options([
                        'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                    ])
                    ->hideFromIndex();
            } else {
                $fields[] = Translatable::make('Contenu', 'description')
                    ->trix()
                    ->hideFromIndex();
            }
        }
        if (config('laravel-cms-nova.fields.block.priority')) {
            $fields[] = Number::make('Priorité', 'priority')
                              ->step(0.01)
                              ->sortable();
        }

        if (config('laravel-cms-nova.fields.block.attachments') && config('laravel-cms-nova.fichier_resource.fichier_register')) {
            $fields[] = MorphMany::make(Attachment::label(), 'attachments', Attachment::class);
        }

        if (config('laravel-cms-nova.fields.block.parent') &&
             config('laravel-cms-nova.archive_resource.archive_register') &&
             config('laravel-cms-nova.post_resource.post_register')) {
            $fields[] = MorphTo::make('Parent', 'owner')->types([
                Archive::class,
                Post::class
            ]);
        }

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
