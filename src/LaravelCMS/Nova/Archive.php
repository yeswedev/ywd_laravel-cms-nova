<?php

namespace YesWeDev\LaravelCMS\Nova;

use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use YesWeDev\Nova\Translatable\Translatable;

use YesWeDev\LaravelCMS\Archive as ArchiveBase;
use Waynestate\Nova\CKEditor;

class Archive extends Page
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = ArchiveBase::class;

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-cms-nova.resources.archive.group');
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return parent::indexQuery($request, $query)->where('type', 'archive');
    }

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('laravel-cms-nova.archive_resource')['archive_display'];
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-cms-nova.archive_names')['archive_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-cms-nova.archive_names')['archive_singularlabel'];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $tabs = [
            'Détails' => []
        ];
        if (config('laravel-cms-nova.fields.archive.title')) {
            $tabs['Détails'][] = Translatable::make('Titre', 'title')
                ->singleLine();
        }
        if (config('laravel-cms-nova.fields.archive.description')) {
            if (config('laravel-cms-nova.fields.archive.wysiwyg') === 'CKEditor') {
                $tabs['Détails'][] = CKEditor::make('Contenu', 'description')
                    ->options([
                        'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                    ]);
            } else {
                $tabs['Détails'][] = Translatable::make('Contenu', 'description')
                    ->trix();
            }
        }
        if (config('laravel-cms-nova.fields.archive.location')) {
            $tabs['Détails'][] = Translatable::make('Lieu', 'location')
                ->singleLine();
        }
        if (config('laravel-cms-nova.fields.archive.url')) {
            $tabs['Détails'][] = Translatable::make('Url', 'url')
                ->singleLine();
        }
        if (config('laravel-cms-nova.fields.archive.reference')) {
            $tabs['Détails'][] = Text::make('Référence', 'reference')
                ->sortable();
        }
        if (config('laravel-cms-nova.fields.archive.publication')) {
            $tabs['Détails'][] = Date::make('Date de publication', 'created_at')
                ->sortable();
        }
        if (config('laravel-cms-nova.fields.archive.priority')) {
            $tabs['Détails'][] = Number::make('Priorité', 'priority')
                ->step(0.01)
                ->sortable();
        }

        if (config('laravel-cms-nova.fields.archive.attachments') && config('laravel-cms-nova.fichier_resource.fichier_register')) {
            $tabs['Détails'][] = MorphMany::make(Attachment::label(), 'attachments', Attachment::class);
        }
        if (config('laravel-cms-nova.fields.archive.blocks') && config('laravel-cms-nova.bloc_resource.bloc_register')) {
            $tabs['Détails'][] = HasMany::make(Block::label(), 'blocks', Block::class);
        }
        if (config('laravel-cms-nova.fields.archive.tags') && config('laravel-cms-nova.filtre_resource.filtre_register')) {
            $tabs['Détails'][] = MorphToMany::make(Tag::label(), 'tags', Tag::class);
        }
        if (config('laravel-cms-nova.fields.archive.children') && config('laravel-cms-nova.post_resource.post_register')) {
            $tabs['Détails'][] = HasMany::make(Post::label(), 'children', Post::class);
        }
        if (config('laravel-cms-nova.fields.archive.parent') && config('laravel-cms-nova.archive_resource.archive_register')) {
            $tabs['Détails'][] = BelongsTo::make(Archive::singularLabel(), 'parent', Archive::class)->hideFromIndex();
        }

        if (config('laravel-cms-nova.seo-fields')) {
            $tabs['Onglet SEO'] = [
                Text::make('H1', 'seo_h1')->hideFromIndex(),
                Text::make('Méta Titre', 'seo_title')->hideFromIndex(),
                Text::make('Méta Description', 'seo_description')->rules("max:160")->hideFromIndex(),
            ];
        }

        return [new Tabs('Tabs', $tabs)];
    }

    /**
     * Build a "relatable" query for the given resource.
     *
     * This query determines which instances of the model may be attached to other resources.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function relatableQuery(NovaRequest $request, $query)
    {
        $parent = array_pop($request->__memoized);
        if ($parent instanceof self::$model) {
            $query = $query->where('id', '!=', $parent->id);
        }

        return $query;
    }
}
