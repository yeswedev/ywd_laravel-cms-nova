<?php

namespace YesWeDev\LaravelCMS;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;

use YesWeDev\LaravelCMS\Nova\Archive;
use YesWeDev\LaravelCMS\Nova\Attachment;
use YesWeDev\LaravelCMS\Nova\Block;
use YesWeDev\LaravelCMS\Nova\Page;
use YesWeDev\LaravelCMS\Nova\Post;
use YesWeDev\LaravelCMS\Nova\Tag;

class LaravelCMSNovaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/laravel-cms-nova.php' => config_path('laravel-cms-nova.php'),
        ], 'laravel-cms-nova');

        $this->publishes([
            __DIR__ . '/../config/ckeditor-field.php' => config_path('ckeditor-field.php'),
        ], 'ckeditor-field');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/laravel-cms-nova.php',
            'laravel-cms-nova'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/../config/ckeditor-field.php',
            'ckeditor-field'
        );

        $resources = [Page::class];

        if (config('laravel-cms-nova.archive_resource.archive_register')) {
            $resources[] = Archive::class;
        }
        if (config('laravel-cms-nova.fichier_resource.fichier_register')) {
            $resources[] = Attachment::class;
        }
        if (config('laravel-cms-nova.bloc_resource.bloc_register')) {
            $resources[] = Block::class;
        }
        if (config('laravel-cms-nova.post_resource.post_register')) {
            $resources[] = Post::class;
        }
        if (config('laravel-cms-nova.filtre_resource.filtre_register')) {
            $resources[] = Tag::class;
        }

        Nova::resources($resources);
    }
}
