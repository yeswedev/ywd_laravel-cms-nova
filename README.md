# YesWeDev - Laravel CMS Nova integration

## Installation

### Nova installation

Install Nova by following [the official installation instructions](https://nova.laravel.com/docs/3.0/installation.html).

### Package installation

1. Add Package with Composer

---

```
composer require yeswedev/laravel-cms-nova
```

2. Package Service Providers

---

In `config/app.php`, section "Package Service Providers":
```
/*
 * Package Service Providers...
 */
YesWeDev\LaravelCMS\LaravelCMSServiceProvider::class,
YesWeDev\LaravelCMS\LaravelCMSNovaServiceProvider::class,
```

3. Publish config

---

Make resources displayed name configurable
use
```
php artisan vendor:publish --tag=laravel-cms-nova
``` 
to publish the config file to `config/laravel-cms-nova.php`

---

CKeditor toolbar configurable
Use
```
php artisan vendor:publish --tag=ckeditor-field 
```
to publish the config file to 
`config/ckeditor-field.php`

---

4. Seo Activation

---

To activate SEO-fields Tab, in your laravel-cms-nova config file enter the key :

```
'seo-fields' => true,
```

## Laravel CMS

For more informations regarding Laravel CMS go to [YesWeDev Laravel CMS](https://framagit.org/yeswedev/ywd_laravel-cms/)
